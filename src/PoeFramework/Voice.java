package PoeFramework;

import ddf.minim.AudioPlayer;

public class Voice {
	private AudioPlayer ply;
	private boolean played;
	public Voice(AudioPlayer ply, boolean played) {
		super();
		this.ply = ply;
		this.played = played;
	}
	public AudioPlayer getPly() {
		return ply;
	}
	public void setPly(AudioPlayer ply) {
		this.ply = ply;
	}
	public boolean isPlayed() {
		return played;
	}
	public void setPlayed(boolean played) {
		this.played = played;
	}
}

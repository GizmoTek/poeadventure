package PoeFramework;

import processing.core.PImage;

public class Item {
	private PImage img;
	private String nam;
	private boolean tkn;//taken
	
	public Item(PImage img, String nam) {
		super();
		this.img = img;
		this.nam = nam;
	}

	public PImage getImg() {
		return img;
	}

	public void setImg(PImage img) {
		this.img = img;
	}

	public String getNam() {
		return nam;
	}

	public void setNam(String nam) {
		this.nam = nam;
	}

	public boolean isTkn() {
		return tkn;
	}

	public void setTkn(boolean tkn) {
		this.tkn = tkn;
	}
	
	
	
}

package PoeFramework;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;

public class Button {
	private Point loc;
	private Dimension dim;
	private Color col;
	private boolean hid;
	private String txt;
	private String act;
	private String var;
	private String val;
	public int State; // 0-Nothing 1-MouseOver 2-MouseDown

	public Button(Point loc, Dimension dim, Color col, boolean hid, String nam, String act, String var, String val) {
		this.loc = loc;
		this.dim = dim;
		this.col = col;
		this.hid = hid;
		this.txt = nam;
		this.act = act;
		this.var = var;
		this.val = val;
		this.State = 0;
	}

	public Button(Point loc, Dimension dim, Color col, String nam, String act, String var, String val) {
		this.loc = loc;
		this.dim = dim;
		this.col = col;
		this.txt = nam;
		this.act = act;
		this.var = var;
		this.val = val;
		this.State = 0;
	}

	public Button(Point loc, Dimension dim, Color col, boolean hid, String nam) {
		this.loc = loc;
		this.dim = dim;
		this.col = col;
		this.hid = hid;
		this.txt = nam;
		this.State = 0;
	}

	public Button(Point loc, Dimension dim, Color col, String nam) {

		this.loc = loc;
		this.dim = dim;
		this.col = col;
		this.txt = nam;
		this.State = 0;
	}

	public Point getLoc() {
		return loc;
	}

	public void setLoc(Point loc) {
		this.loc = loc;
	}

	public Dimension getDim() {
		return dim;
	}

	public void setDim(Dimension dim) {
		this.dim = dim;
	}

	public Color getCol() {
		return col;
	}

	public void setCol(Color col) {
		this.col = col;
	}

	public String getTxt() {
		return txt;
	}

	public void setTxt(String nam) {
		this.txt = nam;
	}

	public String getAct() {
		return act;
	}

	public void setAct(String act) {
		this.act = act;
	}

	public String getVar() {
		return var;
	}

	public void setVar(String var) {
		this.var = var;
	}

	public String getVal() {
		return val;
	}

	public void setVal(String val) {
		this.val = val;
	}

	public boolean isHid() {
		return hid;
	}

	public void setHid(boolean hid) {
		this.hid = hid;
	}

	public int getState() {
		return State;
	}

	public void setState(int state) {
		State = state;
	}

}

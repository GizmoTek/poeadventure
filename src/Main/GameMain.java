package Main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.util.ArrayList;

import PoeFramework.Button;
import PoeFramework.Image;
import PoeFramework.Item;
import PoeFramework.Text;
import PoeFramework.Voice;
import ddf.minim.AudioPlayer;
import ddf.minim.Minim;
import processing.core.PApplet;
import processing.core.PImage;

public class GameMain extends PApplet {

	private byte guiScreen;// Negitive - Menu / Positive - Game // -128 to 127
	private byte guiCache = 0;
	private int frame = 0; // frame of animation of guiScreen
	private byte trys; // attempts of old man
	private int voiceStep = -1;
	private boolean inAction;// animation/ other
	boolean OMRL = false;
	boolean OMRE = false;
	String eol = System.getProperty("line.separator");
	ArrayList<Text> texts = new ArrayList<>(128);
	ArrayList<Image> images = new ArrayList<>(128);
	ArrayList<Button> buttons = new ArrayList<>(128);
	ArrayList<Voice> voices = new ArrayList<>(128);
	ArrayList<Item> items = new ArrayList<>(6);

	/********* Media **********/
	Minim minim;
	// Audio
	AudioPlayer sss;
	AudioPlayer wind;
	AudioPlayer tick;
	AudioPlayer twelve;
	AudioPlayer jhon_moan_loud;
	AudioPlayer jhon_moan_death;
	AudioPlayer pluto;
	AudioPlayer noOutside;
	AudioPlayer para1;
	AudioPlayer para2;
	AudioPlayer para3a;
	AudioPlayer para3b;
	// Visual
	PImage hall;
	PImage omr;
	PImage omrl;
	PImage omrle;
	PImage bedroom;
	PImage arrow;
	PImage back;
	PImage poe;
	PImage tokitchen;
	PImage kitchen;
	PImage knife;
	PImage ugc;

	public static void main(String[] args) {
		PApplet.main("Main.GameMain");
	}

	@Override
	public void settings() {
		this.size(800, 600);
	}

	@Override
	public void setup() {
		this.guiScreen = -128;

		this.minim = new Minim(this);

		// Audio
		this.sss = this.minim.loadFile("data/sound/spookyskeletons.mp3");
		this.wind = this.minim.loadFile("data/sound/wind.mp3");
		this.tick = this.minim.loadFile("data/sound/tick.mp3");
		this.twelve = this.minim.loadFile("data/sound/twelve.mp3");
		this.pluto = this.minim.loadFile("data/sound/pluto.mp3");
		this.noOutside = this.minim.loadFile("data/sound/noOutside.mp3");
		this.para1 = this.minim.loadFile("data/sound/para1-2.mp3");
		this.para2 = this.minim.loadFile("data/sound/para2.mp3");
		this.para3a = this.minim.loadFile("data/sound/para3-1.mp3");
		this.para3b = this.minim.loadFile("data/sound/para3-2.mp3");
		this.jhon_moan_loud = this.minim.loadFile("data/sound/jhon_moan_loud.mp3");
		this.jhon_moan_death = this.minim.loadFile("data/sound/jhon_moan_death.mp3");
		this.voices.add(new Voice(this.para1, false));
		this.voices.add(new Voice(this.para3a, false));
		this.voices.add(new Voice(this.para3b, false));
		// Visual
		this.hall = this.loadImage("data/image/Hall3.png");
		this.omrle = this.loadImage("data/image/OMC.png");
		this.omrl = this.loadImage("data/image/OMR2.png");
		this.omr = this.loadImage("data/image/OMR3.png");
		this.bedroom = this.loadImage("data/image/Bedroom.png");
		this.arrow = this.loadImage("data/image/arrow.png");
		this.back = this.loadImage("data/image/BackArrow.png");
		this.kitchen = this.loadImage("data/image/Kitchen.png");
		this.tokitchen = this.loadImage("data/image/toKitchen.png");
		this.knife = this.loadImage("data/image/knife.png");
		this.poe = this.loadImage("data/image/poe.png");
		this.ugc = this.loadImage("data/image/ugc.png");
		// item
		this.items.add(new Item(this.knife, "knife"));

	}

	@Override
	public void draw() {
		this.background(Color.gray.getRGB());
		this.drawGui();
		this.checkMouse();
		this.update();
	}

	private void drawGui() {
		this.readyGui();
		this.drawImages();
		this.drawButtons();
		this.drawText();
		this.drawItems();
	}

	private void readyGui() {
		this.buttons.clear();
		this.images.clear();
		this.texts.clear();
		switch (this.guiScreen) {
		case -128:
			this.texts.add(new Text(new Point(375, 80), 60, Color.black, "The Tell-Tale Heart Pt.1", false));
			this.images.add(new Image(new Point(50, 200), new Dimension(300, 372), this.poe, false));
			this.buttons.add(new Button(new Point(450, 300), new Dimension(300, 100), Color.gray, "Play", "changeVar", "guiScreen", "2"));
			this.buttons.add(new Button(new Point(450, 450), new Dimension(300, 100), Color.gray, "Credits", "changeVar", "guiScreen", "-127"));
			break;
		case -127:
			this.texts.add(new Text(new Point(400, 80), 60, Color.black, "Credits", false));
			this.texts.add(new Text(new Point(400, 150), 40, Color.black, "Programming: Stephen Toth", false));
			this.texts.add(new Text(new Point(400, 200), 40, Color.black, "Art: Stephen Toth", false));
			this.texts.add(new Text(new Point(400, 250), 40, Color.black, "Sound Director: Stephen Toth", false));
			this.texts.add(new Text(new Point(500, 300), 40, Color.black, "Narrator: Stephen Toth", false));
			this.texts.add(new Text(new Point(500, 350), 40, Color.black, "Old Man: Jhon Manning", false));
			this.texts.add(new Text(new Point(400, 400), 40, Color.black, "Music: TheLivingTombstone", false));

			this.images.add(new Image(new Point(600, 500), new Dimension(200, 100), this.arrow, false));
			this.buttons.add(new Button(new Point(600, 500), new Dimension(200, 100), Color.yellow, true, "Back", "changeVar", "guiScreen", "-128"));
			break;
		case -1:
		case -2:
			this.background(Color.black.getRGB());
			if (!this.jhon_moan_death.isPlaying()) {
				this.texts.add(new Text(new Point(110, 580), 30, Color.white, "(Old man died)", false));
			}

			break;
		case 2:
			// hallway
			this.images.add(new Image(new Point(0, 0), new Dimension(800, 500), this.hall, false));
			this.texts.add(new Text(new Point(150, 20), 40, Color.black, "Night:" + (this.trys + 1), false));
			this.buttons.add(new Button(new Point(165, 205), new Dimension(95, 155), Color.yellow, true, "Outside", "playSound", "", "noOutside"));
			this.buttons.add(new Button(new Point(315, 225), new Dimension(30, 80), Color.yellow, true, "Black Cat", "playSound", "", "pluto"));
			this.buttons.add(new Button(new Point(360, 115), new Dimension(95, 385), Color.yellow, true, "Old Mans Room", "changeVar", "guiScreen", "3"));
			this.buttons.add(new Button(new Point(450, 85), new Dimension(65, 230), Color.yellow, true, "Clock", "playSound", "", "wind"));
			this.buttons.add(new Button(new Point(540, 0), new Dimension(180, 500), Color.yellow, true, "Back To Bed", "changeVar", "guiScreen", "4"));
			this.buttons.add(new Button(new Point(50, 220), new Dimension(105, 195), Color.yellow, true, "Paino", "playSound", "", "sss"));
			this.buttons.add(new Button(new Point(0, 0), new Dimension(45, 500), Color.yellow, true, "To Kitchen", "changeVar", "guiScreen", "5"));
			break;
		case 3:
			// old mans room
			if (this.OMRL) {
				if (this.OMRE) {
					this.images.add(new Image(new Point(0, 0), new Dimension(800, 500), this.omrl, false));
				} else {
					this.images.add(new Image(new Point(0, 0), new Dimension(800, 500), this.omrl, false));
					this.images.add(new Image(new Point(205, 108), new Dimension(103, 56), this.omrle, false));
				}
			} else {
				this.images.add(new Image(new Point(0, 0), new Dimension(800, 500), this.omr, false));
			}

			if (this.OMRL && this.OMRE) {
				this.buttons.add(new Button(new Point(210, 100), new Dimension(95, 60), Color.yellow, true, "Old Man", "changeVar", "guiScreen", "6"));
			}

			this.images.add(new Image(new Point(600, 400), new Dimension(200, 100), this.arrow, false));
			this.buttons.add(new Button(new Point(600, 400), new Dimension(200, 100), Color.yellow, true, "Back", "changeVar", "guiScreen", "2"));
			this.buttons.add(new Button(new Point(100, 170), new Dimension(50, 120), Color.yellow, true, "Lamp", "changeVar", "OMRL", ""));
			break;
		case 4:
			// bedroom
			this.images.add(new Image(new Point(0, 0), new Dimension(800, 500), this.bedroom, false));
			break;
		case 5:
			// kitchen
			this.images.add(new Image(new Point(0, 0), new Dimension(800, 500), this.kitchen, false));
			if (!this.items.get(0).isTkn()) {
				// if knife isnt taken
				this.images.add(new Image(new Point(10, 375), new Dimension(200, 90), this.knife, false));
				this.buttons.add(new Button(new Point(10, 400), new Dimension(200, 90), Color.yellow, true, "Get Knife", "addItem", "", "knife"));
			}

			this.images.add(new Image(new Point(600, 400), new Dimension(200, 100), this.arrow, false));
			this.buttons.add(new Button(new Point(600, 400), new Dimension(200, 100), Color.yellow, true, "Back", "changeVar", "guiScreen", "2"));
			break;

		case 6:

			this.images.add(new Image(new Point(0, 0), new Dimension(800, 500), this.omrl, false));
			this.buttons.add(new Button(new Point(100, 160), new Dimension(255, 295), Color.yellow, true, "Bed", "changeVar", "guiScreen", "-1"));
			if (this.items.get(0).isTkn()) {
				this.buttons.add(new Button(new Point(0, 500), new Dimension(100, 100), Color.yellow, true, "Knife", "changeVar", "guiScreen", "-2"));
			}

			this.texts.add(new Text(new Point(400, 30), 30, Color.white, "Kill the old man with what?", false));
			break;

		case 127:
			this.images.add(new Image(new Point(0, 0), new Dimension(800, 500), this.ugc, false));
			if (this.OMRL) {
				this.texts.add(new Text(new Point(375, 450), 20, Color.BLACK, "You left the light on", false));
			} else {
			}
			break;
		}

		if (this.inAction) {
			this.buttons.add(new Button(new Point(775, 475), new Dimension(25, 25), Color.red, false, "Stop", "stopAction", "", ""));
		}

	}

	private void drawImages() {
		for (int i = 0; i < this.images.size(); i++) {
			Image currImage = this.images.get(i);
			this.image(currImage.getImg(), currImage.getLoc().x, currImage.getLoc().y, currImage.getDim().width, currImage.getDim().height);
		}
	}

	private void drawButtons() {
		for (int i = 0; i < this.buttons.size(); i++) {
			Button currButton = this.buttons.get(i);
			if (!currButton.isHid()) {
				int font = (currButton.getDim().height - currButton.getTxt().length()) / 2;
				this.drawRect(currButton.getLoc().x, currButton.getLoc().y, currButton.getDim().width, currButton.getDim().height, currButton.getCol(), currButton.getTxt(), font);
			}
		}
	}

	private void drawText() {
		for (int i = 0; i < this.texts.size(); i++) {
			Text currText = this.texts.get(i);
			this.textSize(currText.getFont());
			this.fill(currText.getCol().getRGB());
			this.textAlign(CENTER, CENTER);
			this.text(currText.getTxt(), currText.getLoc().x, currText.getLoc().y);
			this.fill(50);
		}

	}

	private void drawItems() {
		for (int i = 0; i < this.items.size(); i++) {
			Item currItem = this.items.get(i);
			if (currItem.isTkn() && (this.guiScreen > 0)) {
				this.image(currItem.getImg(), i * 100, 500, 100, 100);
			}

		}
	}

	private void checkMouse() {
		//System.out.println(mouseX + ", " + mouseY);
		for (int i = 0; i < this.buttons.size(); i++) {
			Button currButton = this.buttons.get(i);
			if ((this.mouseX > currButton.getLoc().x) && (this.mouseX < (currButton.getLoc().x + currButton.getDim().getWidth())) && (this.mouseY > currButton.getLoc().y)
					&& (this.mouseY < (currButton.getLoc().y + currButton.getDim().getHeight()))) {
				currButton.State = 1;
				this.drawTextMouse(currButton, this.mouseX + 10, this.mouseY);
			}
		}
	}

	@Override
	public void mousePressed() {
		if (!this.inAction) {
			for (int i = 0; i < this.buttons.size(); i++) {
				Button currButton = this.buttons.get(i);
				if (currButton.State == 1) {
					currButton.State = 2;
					switch (currButton.getAct()) {

					case "changeVar":
						switch (currButton.getVar()) {
						case "guiScreen":
							this.guiScreen = (byte) Integer.parseInt(currButton.getVal());
							break;
						case "OMRL":
							this.OMRL ^= true;
							break;
						}
						break;
					case "playSound":
						switch (currButton.getVal()) {
						case "sss":
							this.sss.play();
							this.sss.rewind();
							break;
						case "wind":
							this.wind.play();
							this.wind.rewind();
							break;
						case "pluto":
							this.pluto.play();
							this.pluto.rewind();
							break;
						case "noOutside":
							this.noOutside.play();
							this.noOutside.rewind();
							break;
						}
					case "addItem":
						switch (currButton.getVal()) {
						case "knife":
							this.items.get(0).setTkn(true);
						}
						break;
					}

				}
			}
		}

		for (int i = 0; i < this.buttons.size(); i++) {
			Button currButton = this.buttons.get(i);
			if (currButton.State == 1) {
				currButton.State = 2;
				switch (currButton.getAct()) {
				case "stopAction":
					if (this.voiceStep > -1) {
						// System.out.println("yo");
						this.voices.get(this.voiceStep).getPly().pause();
						this.inAction = false;
					}
					break;
				}
			}
		}

	}

	private void update() {
		// System.out.println(guiCache + ", " + guiScreen);
		this.frame++;
		// System.out.println(frame);
		/********** ONCE ***********/
		if (this.guiCache != this.guiScreen) {
			this.frame = 0;
			if (this.tick.isLooping()) {
				this.tick.mute();
				this.tick.play();
			}
			this.tick.unmute();
			this.tick.setGain(-20);
			this.tick.loop();
			switch (this.guiScreen) {
			case -128:
				this.voiceStep = -1;
				break;
			case 2:
				if (this.voiceStep == -1) {
					this.voiceStep = 0;
				}
				break;
			case 3:
				if (this.OMRE) {
					this.twelve.play();
				}
				if (this.voiceStep == 0) {
					this.voiceStep = 1;
				}

				break;
			case 4:
				int i = (int) (this.random(0, 9));
				this.trys++;
				// System.out.println(trys);
				if (this.trys == 7) {
					i = 8;

				}
				// System.out.println(i);
				if (i == 8) {
					this.OMRE = true;
				} else {
					this.OMRE = false;
				}

				break;
			case 127:
				this.jhon_moan_loud.play();
				this.jhon_moan_loud.rewind();
				break;
			case -1:
			case -2:
				for (int k = 0; k < this.items.size(); k++) {
					Item currItem = this.items.get(k);
					currItem.setTkn(false);

				}
				this.jhon_moan_death.play();
				break;

			}
			if ((this.voiceStep > -1) && (this.voices.get(this.voiceStep).isPlayed() == false)) {
				this.voices.get(this.voiceStep).getPly().play();
				this.voices.get(this.voiceStep).getPly().rewind();
				this.voices.get(this.voiceStep).setPlayed(true);
			}
			this.guiCache = this.guiScreen;
		}
		/*********** Multi ***********/
		// System.out.println(OMRL);
		if (this.voiceStep > -1) {
			if (this.voices.get(this.voiceStep).getPly().isPlaying()) {
				this.inAction = true;
			} else {
				this.inAction = false;
			}
		}

		switch (this.guiScreen) {
		case 4:
			if (this.frame >= 100) {
				if (this.OMRL) {
					this.guiScreen = 127;
				} else {
					this.guiScreen = 2;
				}
				this.twelve.rewind();
			}
			break;
		}

	}

	private void drawTextMouse(Button currButton, int x, int y) {
		this.textSize(15);
		this.fill(50);
		this.textAlign(LEFT, CENTER);
		this.text(currButton.getTxt(), x, y);

	}

	private void drawRect(float xPos, float yPos, float width, float height, Color color, String text, int size) {
		this.fill(color.getRGB());
		this.rect(xPos, yPos, width, height);
		this.textSize(size);
		this.fill(50);
		this.textAlign(CENTER, CENTER);
		this.text(text, xPos, yPos, width, height);
	}

}